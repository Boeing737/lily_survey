//return :: String
function getParameter() {
    return location.search.split("?")[1];
}

//return :: Array
function setGetParameter() {
    var param_List = [];
    var StringGetParameter = getParameter();
    if (StringGetParameter == null) {
        return null;
    }
    StringGetParameter.split("&").forEach(function (Param) {
        var param_Arr = Param.split("=");
        param_List[param_Arr[0]] = param_Arr[1];
    });
    return param_List;
}

//return :: number
function getSurveyIndex() {
    var SurveyIndex = setGetParameter();
    //get을 통해 얻은 데이터가 없거나, 숫자가 아닐 경우 예외
    if ((SurveyIndex == null) || SurveyIndex["SurveyIndex"] == "" || isNaN(SurveyIndex = Number(SurveyIndex["SurveyIndex"])) || SurveyIndex < 0) {
        alert("정보를 가져오는데 실패하였습니다.");
        loadManagePage();
    }
    return SurveyIndex;
}