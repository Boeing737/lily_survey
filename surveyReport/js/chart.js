var graphJSON = {
    "type": "pie",
    "startDuration": 0,
     "theme": "light",
    "addClassNames": true,
    "legend":{
         "position":"right",
      "marginRight":100,
      "autoMargins":false
    },
    "innerRadius": "50%",
    "defs": {
      "filter": [{
        "id": "shadow",
        "width": "200%",
        "height": "200%",
        "feOffset": {
          "result": "offOut",
          "in": "SourceAlpha",
          "dx": 0,
          "dy": 0
        },
        "feGaussianBlur": {
          "result": "blurOut",
          "in": "offOut",
          "stdDeviation": 5
        },
        "feBlend": {
          "in": "SourceGraphic",
          "in2": "blurOut",
          "mode": "normal"
        }
      }]
    },
    "dataProvider": [{
      "answer": "매우 그렇다 (5)",
      "count": 11,
      "color": "#67B7DC"
    }, {
      "answer": "그렇다 (4)",
      "count": 4,
      "color": "#84B761"
    }, {
      "answer": "보통이다 (3)",
      "count": 2,
      "color": "#FDD400"
    }, {
      "answer": "그렇지 않다 (2)",
      "count": 0,
      "color": "#F29661"
    }, {
      "answer": "전혀 그렇지 않다 (1)",
      "count": 0,
      "color": "#CC4748"
    }],
    "valueField": "count",
    "titleField": "answer",
    "colorField": "color",
    "labelColorField": "color",
    "export": {
        // allows to download 
      "enabled": false
    }
  };