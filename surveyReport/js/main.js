var counts = {
    count5: 0,
    count4: 0,
    count3: 0,
    count2: 0,
    count1: 0
}
var score_count = [];
var chartId = 'chart';
var graphCount = 1;
var survey_result = document.getElementById('survey_result');
var final_score = 0;
var title;

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        showLoading();
        startgetDB().then(function (success) {
            var Survey = this.getSurvey();
            console.log("설문 조사 타이틀 : " + getSurveyTitleInDB(Survey));
            document.getElementById('total_evaluation').style.display = 'block';
            createData(getGroupsInDB(Survey));
            this.onLoadPage();
            noneLoading();
        });
        console.log("success");
    } else {
        alert("로그인이 필요합니다.");
        loadSignInPage();
    }
});

function onLoadPage() {
    var survey = this.getSurvey();
    title = getSurveyTitleInDB(survey);
    var lecturer = getLectureInDB(survey) + " 강사";
    var date = getDateInDB(survey);

    $('#title').text(title);
    $('#lecturer_name').text(lecturer);
    $('#date').text(date);
}

//return :: Object
function getSurvey() {
    var SurveyIndex = getSurveyIndex();
    var SurveyObject = getSurveyList(getSignInEmail())[SurveyIndex];
    var Survey = getSurveyInDB(SurveyObject);
    return Survey;
}

//return :: number
function getSurveyIndex() {
    var SurveyIndex = setGetParameter();
    //get을 통해 얻은 데이터가 없거나, 숫자가 아닐 경우 예외
    if ((SurveyIndex == null) || SurveyIndex["SurveyIndex"] == "" || isNaN(SurveyIndex = Number(SurveyIndex["SurveyIndex"])) || SurveyIndex < 0) {
        alert("정보를 가져오는데 실패하였습니다.");
        loadManagePage();
    }
    return SurveyIndex;
}

function createData(Groups){
    Groups.forEach(function(Group){
        console.log("----------------");
        console.log("Group : " + Group["Group"]);
        console.log("----------------");
        var Categories = getCategoriesInDB(Group);
        var CategoryAvg = 0;
        this.createHeader(Group["Group"], "isGroup");

        Categories.forEach(function(Category){
            var itemAvg = 0;
            var totalScore = 0;
            var question_index = 1;
            var Items = getItemsInDB(Category);
            this.createHeader(Category["Category"], "isCategory");

            Items.forEach(function(Item){
                var Scores = getScore(Item);
                var surveyeeCount = Scores.length;
                var total = Scores.reduce(function(a, b) {
                    return parseInt(a) + parseInt(b);
                });
                itemAvg += total;
                this.countDuplicate(Scores);
                this.createEachItemGraph(counts, Item["Item"], question_index);
                question_index += 1;
                $('#survey_count').text("설문 인원: " + Scores.length + "명");
                totalScore = (itemAvg / (Items.length * surveyeeCount)).toFixed(2); //leave decimal to .00
                for(var key in counts) {
                    counts[key] = 0;
                }
            })
            CategoryAvg += parseFloat(totalScore);
            console.log(Category["Category"] + " : " + totalScore);
        })
        CategoryAvg = CategoryAvg / Categories.length;
        this.createBar(CategoryAvg, Group['Group']);
        console.log(Group["Group"] + " : " + CategoryAvg);
    })
    final_score  = (final_score / Groups.length).toFixed(1);
    this.createTrophy(final_score);
    $('#total_score').text(final_score);
    if(confirm("항목별 통계를 다운받으시겠습니까?")){
        arrayToCSV(score_count);
    }
}

function arrayToCSV(twoDiArray) {
    var csvRows = [];
    for (var i = 0; i < twoDiArray.length; ++i) {
        for (var j = 0; j < twoDiArray[i].length; ++j) {
            twoDiArray[i][j] = '\"' + twoDiArray[i][j] + '\"';
        }
        csvRows.push(twoDiArray[i].join(','));
    }

    var csvString = csvRows.join('\r\n');
    var a = document.createElement('a');
    a.href = 'data:attachment/csv,' + csvString;
    a.target = '_blank';
    a.download = 'counts.csv';

    document.body.appendChild(a);
    a.click();
}

function createHeader(h_name, type) {
    var container = document.createElement('div');
    var text = document.createElement('span');

    if(type === "isGroup"){
        container.className = 'group_header';
    }
    else{
        container.className = 'category_header';
    }
    text.innerHTML = h_name;
    container.appendChild(text);
    survey_result.appendChild(container);
}

function calculatePercent(data) {
    var result = parseInt((data / 5) * 100);
    //var result = ((data / 5) * 100).toFixed(1); // decimal result
    final_score += result;
    return result;
}

function createBar(data, group) {
    var container = document.getElementsByClassName('bar_container')[0];
    var bar = document.createElement('div');
    var result = document.createElement('div');
    var bar_text = document.createElement('span');
    var percent_text = document.createElement('span');
    var background = document.createElement('div');
    var percent = this.calculatePercent(data);

    bar.className = 'bar';
    result.className = 'bar_result';
    bar_text.className = 'bar_group';
    percent_text.className = 'group_percent v_middle';
    background.className = 'bar_background';

    bar_text.innerHTML = group;
    result.style.width = (percent * 4) + 'px';
    percent_text.innerHTML = percent + "%";

    result.appendChild(bar_text);
    bar.appendChild(result);
    bar.appendChild(background);
    bar.appendChild(percent_text);
    container.appendChild(bar);
}

function createTrophy(score) {
    var trophy = document.getElementById('trophy');
    var path = "image/trophy/";
    if(score >= 90){
        trophy.src = path + "icon_A_GoldTrophy.png";
    }
    else if(score >= 80){
        trophy.src = path + "icon_B_SilverTrophy.png";
    }
    else{
        trophy.src = path + "icon_C_BronzeTrophy.png";
    }

}

function createEachItemGraph(counts, question, index) {
    var result_index = 0;
    var graphContainer = document.createElement('div');
    graphContainer.id = 'chart' + graphCount; // ++1
    graphContainer.className = "graph";
    graphCount += 1;
    survey_result.appendChild(graphContainer);
    var copyOfGraph = JSON.parse(JSON.stringify( graphJSON ));
    for(var key in counts){
        if (counts.hasOwnProperty(key)) {
            copyOfGraph.dataProvider[result_index].count = counts[key];
            result_index +=1;
        }
    }
    this.createChart(graphContainer.id, copyOfGraph);
    this.displayQuestion(question, graphContainer, index);
}

function displayQuestion(question, graphContainer, index){
    var text_container = document.createElement('div');
    var question_wrapper = document.createElement('span');
    text_container.className = 'question_container';
    question_wrapper.innerHTML = index + '. ' + question;
    question_wrapper.className = 'question';
    graphContainer.appendChild(text_container);
    text_container.appendChild(question_wrapper);
}

function countDuplicate(Scores) {
    var row = [];
    for(var i = 0; i < Scores.length; i++){
        if(Scores[i] == '1'){
            counts.count5 += 1;
        }
        if(Scores[i] == '2'){
            counts.count4 += 1;
        }
        if(Scores[i] == '3'){
            counts.count3 += 1;
        }
        if(Scores[i] == '4'){
            counts.count2 += 1;
        }
        if(Scores[i] == '5'){
            counts.count1 += 1;
        }
    }
    row.push(counts.count5,counts.count4,counts.count3,counts.count2,counts.count1);
    score_count.push(row);
    console.log(((counts.count5 * 5)+(counts.count4 * 4)+(counts.count3 * 3)+(counts.count2 * 2)+(counts.count1 * 1)) /26);
}

function createChart(id, graph) {
    var chart = AmCharts.makeChart(id, graph);
    chart.addListener("init", handleInit);
    chart.addListener("rollOverSlice", function(e) {
        handleRollOver(e);
    });
}
function handleInit() {
    chart.legend.addListener("rollOverItem", handleRollOver);
}
  
function handleRollOver(e) {
    var wedge = e.dataItem.wedge.node;
    wedge.parentNode.appendChild(wedge);
}

function printButton() {
    var print_button = document.getElementById('print');
    var png_button = document.getElementById('download');
    print_button.style.display = 'none';
    png_button.style.display = 'none';
    setTimeout(window.print(),5000);
    print_button.style.display = 'block';
    png_button.style.display = 'block';
}

function onPrint() {
    var print_button = document.createElement('a');
    var background = document.createElement('div');
    print_button.className = 'option_button';
    print_button.id = 'print';
    print_button.innerHTML = '출력';
    print_button.setAttribute('onclick','printButton()');

    var png_button = document.createElement('a');
    png_button.className = 'option_button';
    png_button.innerHTML = '파일 저장';
    png_button.id = 'download';

    png_button.addEventListener('click', function() {
        downloadCanvas(this);
    }, false);

    background.className = "canvas_background";
    background.setAttribute("onclick", "closeCanvas(event)");
    html2canvas(document.querySelector("#total_evaluation")).then(canvas => {
        canvas.id = "canvas";
        background.appendChild(canvas)
        background.appendChild(png_button);
        background.appendChild(print_button);
    });
    document.body.appendChild(background);
}

function closeCanvas(event) {
    var current = event.target;
    if(current.tagName === 'A' || current.tagName === 'a'){
        return;
    }
    else{
        document.body.removeChild(current);
    }
}

function downloadCanvas(link) {
    link.href = document.getElementById('canvas').toDataURL();
    link.download = title + '.png';
}