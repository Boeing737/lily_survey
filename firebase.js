// Initialize Firebase
var config = {
    apiKey: "AIzaSyAZlKdPb76kOu0yfhMEs47blsb37Y-a7Mg",
    authDomain: "lilysurvey-ee338.firebaseapp.com",
    databaseURL: "https://lilysurvey-ee338.firebaseio.com",
    projectId: "lilysurvey-ee338",
    storageBucket: "lilysurvey-ee338.appspot.com",
    messagingSenderId: "1042385941176"
};

firebase.initializeApp(config);

function isSignIn() {
    var user = firebase.auth().currentUser;

    if (user) {
        return true;
    } else {
        return false;
    }
}

function getSignInEmail() {
    return firebase.auth().currentUser.email;
}