
$(document).ready(function(){
    $('#email_text, #password_text').keypress(function(event){
        if(event.keyCode === 13){
            $('#login_button').click();
        }
    });
    $('#s_email_text, #s_password_text, #s_password_check').keypress(function(event){
        if(event.keyCode === 13){
            $('#signup').click();
        }
    });
});

firebase.auth().onAuthStateChanged(function (user) {
    startgetDB();
    if (user) {
        showLoading();
        loadManagePage();
    } else {
        console.log("비로그인");
    }
});

function onSignIn() {
    var email = getLoginEmail();
    var password = getLoginPassword();
    showLoading();
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function (success) {
            loadManagePage();
        })
        .catch(function (error) {
            // Handle Errors here.
            $('#confirm_login_message').show();
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode);
            console.log(errorMessage);
            noneLoading();
        });
}

function onSignUp() {
    var email = getEmail();
    var password = getPassword();
    var password_check = getPasswordCheck();
    // showLoading();
    if(password !== password_check){
        $('#confirm_signup_message').show();
        noneLoading();
        return;
    }
    firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function (success) {
            setEmailToDatabase()
                .then(function (success) {
                    //TODO :: 나중에 회원가입 완료 풍부하게 수정
                })
                .catch(function(error){
                    console.log(error);
                });
        })
        .catch(function (error) {
            // Handle Errors here.
            $('#confirm_signup_message').show();
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode);
            console.log(errorMessage);
            noneLoading();
        });
}

function getEmail() {
    return $("#s_email_text").val();
}

function getPassword() {
    return $("#s_password_text").val();
}

function getPasswordCheck() {
    return $("#s_password_check").val();
}

function onSelectSignUp() {
    $('#confirm_login_message').hide();
    var signup_section = document.getElementsByClassName('signup')[0];
    signup_section.style.visibility = 'visible';
    signup_section.style.opacity = 1;
}

function onCloseSignUp() {
    $('#confirm_signup_message').hide();
    var signup_section = document.getElementsByClassName('signup')[0];
    signup_section.style.visibility = 'hidden';
    signup_section.style.opacity = 0;
 
}

function getLoginEmail() {
    return $("#email_text").val();
}

function getLoginPassword() {
    return $("#password_text").val();
}