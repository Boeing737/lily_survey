// TODO :: alert, confirm 기능 대신 costomized modal display

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        showLoading();
        startgetDB().then(function(success){
            loadSurvey();
        })
    } else {
        alert("로그인이 필요합니다.");
        loadSignInPage();
        noneLoading();
    }
});

function onSignOut() {
    if(confirm('로그아웃 하시겠습니까?')){
        showLoading();
        firebase.auth().signOut()
            .then(function (success) {
                loadSignInPage();
            })
            .catch(function (error) {
                console.log(error);
                noneLoading();
            })
    }
}

// TODO :: 작성한 설문 LIST를 뿌려주는 함수
function loadSurvey() {
    // TODO :: 사용사 설문 list를 얻어온 후 각각 Data를 가져온다.
    var SurveyList = getSurveyList(getSignInEmail());
    SurveyList.forEach(function (Survey) {
        var Survey = getSurveyInDB(Survey)
        var SurveyTitle = getSurveyTitleInDB(Survey);
        var Lecture = getLectureInDB(Survey);
        var date = getDateInDB(Survey);
        var SurveyeeCount = getSurveyeeCountInDB(Survey);
        console.log("----------------");
        console.log(SurveyTitle);
        console.log(Lecture);
        console.log(date);
        console.log(SurveyeeCount);
        console.log("----------------");
        createSurveyBox(SurveyTitle, Lecture, date);
    });
    noneLoading();
}

function createSurveyBox(title, lecturer, date) {
    var survey_area = document.getElementById('survey_area');
    var box_container = document.createElement('div');
    var info_container = document.createElement('div');
    var title_text = document.createElement('p');
    var lecturer_text = document.createElement('p');
    var date_text = document.createElement('p');

    box_container.className = 'created_survey create_box';
    info_container.className = 'text_container';

    title_text.innerHTML = title;
    lecturer_text.innerHTML = lecturer;
    date_text.innerHTML = date;

    info_container.appendChild(title_text);
    info_container.appendChild(lecturer_text);
    info_container.appendChild(date_text);
    box_container.appendChild(info_container);
    survey_area.appendChild(box_container);
    createOptions(info_container);
}

function createOptions(container) {
    var upper = setUpperAttr();
    var lower = setLowerAttr();
    var button_container = setButtonAttr();
    container.appendChild(upper);
    container.appendChild(lower);
    container.appendChild(button_container);
}

function setUpperAttr() {
    var upper = document.createElement('div');
    upper.className = 'slide_contents upper';
    upper.setAttribute('onclick', 'openResult(event)');
    return this.setUpperContent(upper);
}

function setLowerAttr() {
    var lower = document.createElement('div');
    lower.className = 'slide_contents lower';
    lower.setAttribute('onclick', 'openInput(event)');
    return this.setLowerContent(lower);
}

function setButtonAttr() {
    var button_container = document.createElement('div');
    button_container.className = 'option_button';
    return this.setButtonContent(button_container);
}

function setUpperContent(upper) {
    var view_survey_text = document.createElement('p');
    var report = document.createElement('img');
    view_survey_text.innerHTML = '설문 결과 보기';
    report.className = 'report';
    report.src = 'image/icon_view_chart.png';
    upper.appendChild(report);
    upper.appendChild(view_survey_text);
    return upper;
}

function setLowerContent(lower) {
    var input_result_text = document.createElement('p');
    var input = document.createElement('img');
    input_result_text.innerHTML = '설문 결과 입력하기';
    input.className = 'input';
    input.src = 'image/icon_insert_answer.png';
    lower.appendChild(input);
    lower.appendChild(input_result_text);
    return lower;
}

function setButtonContent(container) {
    var edit = document.createElement('img');
    var remove = document.createElement('img');
    var edit_message = document.createElement('img');
    var delete_message = document.createElement('img');
    var edit_wrapper = document.createElement('div');
    var delete_wrapper = document.createElement('div');

    edit.className = 'edit_button lower_option';
    remove.className = 'delete_button lower_option';
    edit_message.className = 'edit_box';
    delete_message.className = 'delete_box';
    edit_wrapper.className = 'edit_wrapper';
    edit_wrapper.setAttribute('onclick', 'openModify(event)');
    delete_wrapper.className = 'delete_wrapper';
    remove.setAttribute('onclick', 'deleteSurvey(event);');
    this.setImagePath(edit, remove, edit_message, delete_message);

    edit_wrapper.appendChild(edit);
    edit_wrapper.appendChild(edit_message);
    delete_wrapper.appendChild(remove);
    delete_wrapper.appendChild(delete_message);
    container.appendChild(edit_wrapper);
    container.appendChild(delete_wrapper);
    return container;
}

function setImagePath(edit, remove, edit_box, delete_box) {
    edit.src = './image/icon_edit.png';
    remove.src = './image/icon_delete.png';
    edit_box.src = './image/icon_edit_message.png';
    delete_box.src = './image/icon_delete_message.png';
}

function getSurvey(index) {
    var SurveyObject = getSurveyList(getSignInEmail())[index];
    var Survey = getSurveyInDB(SurveyObject);
    return Survey;
}

function openResult(event) {
    var click_index = getInputIndex(event);
    var current = $(".upper").index(click_index);
    var result_count = getSurveyeeCountInDB(this.getSurvey(current));
    if(result_count > 0){
        loadSurveyReportPage(current);
    }
    else{
        alert('설문 결과를 먼저 입력하세요.');
    }
    // alert($(".upper").index(click_index));
}

function openInput(event) {
    var click_index = getInputIndex(event);
    var current = $(".lower").index(click_index);
    loadInputResultPage(current);
}

function openModify(event){
    var click_index = getInputIndex(event);
    var current = $(".edit_wrapper").index(click_index);
    console.log(current);
    loadModifySurveyPage(current);
}

function getInputIndex(event) {
    var clicked = event.target;
    var clicked_tag = clicked.tagName;
    if(clicked_tag === 'IMG' || clicked_tag === 'P'){
        clicked = event.target.parentNode;
    }
    return clicked;
}

function deleteSurvey(event) {
    var click_index = getInputIndex(event);
    var current = $('.delete_wrapper').index(click_index);
    if(confirm('해당 설문을 삭제하시겠습니까?')){
        removeSurvey(current);
    }
}

function removeSurvey(index){
    showLoading();
    setSurveyToDatabase("del", index)
        .then(function(success){
            location.reload();
        })
        .catch(function(error){
            console.log(error);
            noneLoading();
        });
}