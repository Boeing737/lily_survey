function setEmailToDatabase() {
    var Database = firebase.database().ref('user');
    var UserList = getUserList();
    var upDateusers = onAddEmail(UserList);
    return Database.set(upDateusers);
}

function setSurveyToDatabase(type, index) {
    var email = getSignInEmail();
    var SurveyList = getSurveyList(email);
    var SurveyListIndex = getSurveyListIndex(email);
    var Database = firebase.database().ref('user/' + SurveyListIndex + '/SurveyList');
    var upDateSurvey;
    if (type === "add") {
        if(index == null){
            upDateSurvey = onAddSurvey(SurveyList)
        }else{
            upDateSurvey = onModifySurvey(SurveyList, index);
        }
    } else if (type === "del") {
        upDateSurvey = onDelSurvey(SurveyList, index);
    }
    console.log(upDateSurvey);
    return Database.set(upDateSurvey);
}

function updateSurveyToDatabase() {
    var email = getSignInEmail();
    var SurveyListIndex = getSurveyListIndex(email);
    var SurveyIndex = getSurveyIndex();
    var Database = firebase.database().ref('user/' + SurveyListIndex + '/SurveyList/' + SurveyIndex + '/Survey');
    var upDateSurvey = inputScore();
    return Database.set(upDateSurvey);
}

//TODO :: update가 안정적 추후 변경 필요
function onAddEmail(UserList) {
    UserList.push({
        userEmail: getEmail()
    });

    return UserList;
}

function onAddSurvey(SurveyList) {
    SurveyList.push(MakeSurveyJSON());
    return SurveyList;
}

function onDelSurvey(SurveyList, index) {
    SurveyList.splice(index, 1);
    return SurveyList;
}

function onModifySurvey(SurveyList, index) {
    SurveyList[index] = MakeSurveyJSON();
    return SurveyList;
}