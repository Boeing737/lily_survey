// TODO :: 중복되는 Sign out 기능에 대한 js 파일 통합 관리 및 Refactoring

// 가져올 데이터: 해당 설문에 대한 주제, 강사, 날짜, 설문항목 개수
var result_list = [];

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        showLoading();
        startgetDB().then(function (success) {
            console.log("설문 조사 타이틀 : " + getSurveyTitleInDB(this.getSurvey()));
            console.log("설문 조사 인원 : " + getSurveyeeCountInDB(this.getSurvey()));
            console.log("설문 조사 내용 수 : " + this.getItemNum());
            this.getScoreList();
            this.onLoadPage();
            noneLoading();
        });
        console.log("success");
    } else {
        alert("로그인이 필요합니다.");
        loadSignInPage();
    }
});

function onSubmit() {
    var count = document.getElementById('survey_count').innerHTML;
    if(parseInt(count) === 1){
        alert('입력된 결과가 없습니다.');
        return;
    }
    updateSurveyToDatabase()
    .then(function(){
        if(confirm('결과를 바로 확인하시겠습니까?')){
            loadSurveyReportPage(getSurveyIndex());
            return;
        }
        loadManagePage();
    })
    .catch(function(){
        noneLoading();
    })
}

//실제 데이터 추가
function onAddResult() {
    var input_result = inputParser();
    var total_result_length = $(".result").length;
    var input_length = input_result.length;

    if(total_result_length !== input_length){
        alert('값을 모두 입력해주세요.');
        return $(".result")[input_length].focus();
    }
    result_list.push(input_result);
    this.getResultList();
    this.resetResultData();
    $(".result")[0].focus();
}

//return :: Array
function inputParser() {
    var result_text = getResultData();
    var input_array = [];
    result_text.forEach(function(result){
        input_array.push(result.value);
    });
    return input_array;
}

// return :: Array
function getResultData() {
    var result_array = [];
    var result = $(".result");
    for(var i = 0; i<result.length; i++){
        if(result[i].value !== ''){
            result_array.push(result[i]);
        }
    }
    return result_array;
}

function resetResultData() {
   getResultData().forEach(function(result){
       result.value = "";
   })
}

//return :: Object
function getSurvey() {
    var SurveyIndex = getSurveyIndex();
    var SurveyObject = getSurveyList(getSignInEmail())[SurveyIndex];
    var Survey = getSurveyInDB(SurveyObject);
    return Survey;
}

function inputScore() {
    var Survey = this.getSurvey();
    var GroupList = getGroupsInDB(Survey);

    result_list.forEach(function (result_arr) {
        GroupList.forEach(function (Group) {
            var CategoryList = getCategoriesInDB(Group);
            CategoryList.forEach(function (Category) {
                var ItemList = getItemsInDB(Category);
                ItemList.forEach(function (Item) {
                    if (Item["Score"] == null) {
                        Item["Score"] = [];
                    }
                    else if (!$.isArray(Item["Score"])) {
                        // Item["Score"] = getArrayForJson(Item["Score"]);
                        Item["Score"] = [];
                    }
                    console.log(Item["Score"].push(result_arr.shift()));
                })
            })
        })
    })
    Survey["SurveyeeCount"] = result_list.length;
    return Survey;
}

//return :: Array
function getItemsText() {
    var Survey = this.getSurvey();
    var GroupList = getGroupsInDB(Survey);
    var Items = [];
    GroupList.forEach(function (Group) {
        var CategoryList = getCategoriesInDB(Group);
        CategoryList.forEach(function (Category) {
            var ItemList = getItemsInDB(Category);
            ItemList.forEach(function (Item) {
                Items.push(Item["Item"]);
                // console.log(Item["Item"]);
            })
        })
    })
    return Items;
}

function getItemNum() {
    return this.getItemsText().length;
}

function getScoreList() {
    var Survey = getSurvey();
    var GroupList = getGroupsInDB(Survey);

    GroupList.forEach(function (Group) {
        var CategoryList = getCategoriesInDB(Group);
        CategoryList.forEach(function (Category) {
            var ItemList = getItemsInDB(Category);
            ItemList.forEach(function (Item) {
                var Scores = getArrayForJson(Item["Score"]);
                Scores.forEach(function (Score, index) {
                    if (result_list[index] == null) {
                        result_list[index] = new Array();
                    }
                    result_list[index].push(Score);
                })
            })
        })
    })
    this.getResultList();
}

function getResultList() {
    var current_index = 0;
    $('#question_result').empty();
    result_list.forEach(function (result, index) {
        current_index = index + 1;
        console.log("No. " + current_index + " | " + result);
        if(current_index !== 0){
            this.insertInputData(current_index, result_list);
        }
    });
    
}

function createInputHistory(index, list) {
    var container = document.getElementById('question_result');
    var row = document.createElement('div');
    var count_container = document.createElement('div');
    var count = document.createElement('span');

    row.setAttribute('class', 'added_result_container');
    count_container.setAttribute('class', 'survey_count');
    count.innerHTML = index + '.';
    count_container.appendChild(count);
    row.appendChild(count_container);
    container.appendChild(row);
    container.scrollTop = container.scrollHeight; //always focus last element
    console.log(container.scrollHeight)
}

function insertInputData(index, list) {
    var current_survey_number = index + 1;
    $('#survey_count').text(current_survey_number);
    
    var class_index = index - 1;
    this.createInputHistory(index);

    for(var i = 0; i < list[class_index].length; i++){
        this.createSpan(list[class_index][i], class_index);
    }
    $(".added_result_container")[class_index].focus();

}

function createSpan(result, index) {
    var container = document.getElementsByClassName('added_result_container')[index];
    var data_container = document.createElement('div');
    var data = document.createElement('span');
    data_container.setAttribute('class', 'table_content');
    data.innerHTML = result;
    data_container.appendChild(data);
    container.appendChild(data_container);
}

function removeResult(index) {
    result_list.splice(index, 1);
    this.getResultList();
}

function onLoadInputBox(index) {
    var box_container = document.getElementById('input_scroll');
    var button = document.getElementById('submit');
    var container = document.createElement('div');
    var result_index = document.createElement('p');
    var input_box = document.createElement('input');

    result_index.innerHTML = index;
    container.setAttribute('class', 'input_container');
    result_index.setAttribute('class', 'result_index');
    input_box.maxLength = 1;
    input_box.setAttribute('class', 'result');
    input_box.setAttribute('type', 'input');
    input_box.setAttribute('onkeydown', 'return onlyNumber(event)');
    input_box.setAttribute('onkeyup', 'removeChar(event)');

    // input_box.setAttribute('onkeypress', 'focusNext(event)');
    // input_box.setAttribute('onkeyup', 'focusPrev(event)');

    container.appendChild(result_index);
    container.appendChild(input_box);
    box_container.appendChild(container);
}

function tableIndex(index) {
    var container = document.getElementById('question_index');
    var index_container = document.createElement('div');
    var index_span = document.createElement('span');

    index_container.setAttribute('class', 'index_container');
    index_span.innerHTML = index + '.';
    index_container.appendChild(index_span);
    container.appendChild(index_container);
}

function onLoadPage() {
    var survey = this.getSurvey();
    var title = getSurveyTitleInDB(survey);
    var lecturer = getLectureInDB(survey);
    var date = getDateInDB(survey);

    $('#title').text(title);
    $('#lecturer').text(lecturer);
    $('#date').text(date);

    var count = this.getItemNum();
    for (var i = 1; i <= count; i++) {
        this.onLoadInputBox(i);
        this.tableIndex(i);
    }
}

function onlyNumber(event) {
    var input = event.target;
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    // if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39) {
    //     return;
    // }
    if((keyID >= 48 && keyID <=53) || keyID === 13 || keyID === 8){
        return;
    }
    else {
        alert('0 ~ 5 사이의 점수를 입력하세요.');
        return false;
    }
}

function removeChar(event) {
    var input = event.target;
    var this_index = $('.result').index(input);
    var total_length = $('.result').length;
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if (keyID == 8 || keyID == 37) {
        if (this_index === 0) {
            return;
        }
        $('.result')[this_index - 1].focus();
        return;
    }else if((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 39){
        if ((this_index + 1) === total_length) {
            return;
        }
        if(input.value == ""){
            return;
        }
        $('.result')[this_index + 1].focus();
        return;
    }
    else if(keyID === 13){ //when press enter
        this.onAddResult();
    }
    else {
        event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }
}