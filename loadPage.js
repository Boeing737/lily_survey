//var rootURL = "http://localhost/lily_survey";
var rootURL = "http://13.124.150.80/survey"

function loadManagePage() {
    location.href = rootURL+"/manageSurvey/index.html";
}

function loadSignInPage() {
    location.href = rootURL+"/index.html";
}

function loadCreateSurveyPage() {
    location.href = rootURL+"/createSurvey/index.html";
}

function loadModifySurveyPage(SurveyIndex){
    location.href = rootURL+"/createSurvey/index.html?SurveyIndex="+SurveyIndex;
}

function loadInputResultPage(SurveyIndex){
    location.href = rootURL+"/inputResult/index.html?SurveyIndex="+SurveyIndex;
}

function loadSurveyReportPage(SurveyIndex){
    location.href = rootURL+"/surveyReport/index.html?SurveyIndex="+SurveyIndex;
}

function showLoading(){
    document.getElementsByClassName("animationload")[0].style.display = "block";
}

function noneLoading(){
    document.getElementsByClassName("animationload")[0].style.display = "none";
}