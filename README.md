# Survey

### page

createSurvey - main.js

manageSurvey - main.js

signin - main.js

inputResult - main.js

surveyReport - main.js

### JS 

firebase.js

readDB.js

SurveyParser.js

writeDB.js

getparameter.js

loadPage.js

## createSurvey

### main.js

firebase.auth().onAuthStateChanged()

setSurveyList()

getSurvey()

onSelect(event)

showGroupList()

showCategoryList(Group)

showItemList(Group, Category)

isDuplicate(input, array)

onAddGroup(event)

onAddCategory(event)

onAddItem(event)

onAddData(selected, input, event)

createGroupOrCategory(wrapper, input, remove)

createItemContents(input, item_wrapper, this_class, remove)

getIndexOfItem(class_name)

AddGroup(GroupTtite)

AddCategory(GroupTitle, CategoryTitle)

AddItem(GroupTitle, CategoryTitle, ItemTitle)

getCategories(GroupTitle)

getItems(GroupTitle, CategoryTitle)

getSurveyTitle()

getHost()

getLecture()

getDate()

getGroupTitleList()

getCategoryTitleList(GroupTitle)

getItemTitleList(GroupTitle, CategoryTitle)

MakeSurveyJSON()

checkDateFormat(input_date)

validationCheck()

checkEmptyElement()

onSubmit()

onClickRemove(event)

removeGroup(index)

removeCategory(index)

removeItem(index)

getKeyID(event)

## manageSurvey

### main.js

firebase.auth().onAuthStateChanged()

onSignOut()

loadSurvey()

createSurveyBox(title, lecturer, date)

createOptions(container)

setUpperAttr()

setLowerAttr()

setButtonAttr()

setUpperContent(upper)

setLowerContent(lower)

setButtonContent(container)

setImagePath(edit, remove, edit_box, delete_box)

getSurvey(index)

openResult(event)

openInput(event)

openModify(event)

getInputIndex(event)

deleteSurvey(event)

removeSurvey(index)

## signin

### main.js

firebase.auth().onAuthStateChanged()

onSignIn()

onSignUp()

getEmail()

getPassword()

getPasswordCheck()

onSelectSignUp()

onCloseSignUp()

getLoginEmail()

getLoginPassword()

## inputResult

### main.js

onSubmit()

onAddResult()

inputParser()

getResultData()

resetResultData()

getSurvey()

inputScore()

getItemsText()

getItemNum()

getScoreList()

getResultList()

createInputHistory(index, list)

insertInputData(index, list)

createSpan(result, index)

removeResult(index)

onLoadInputBox(index)

tableIndex(index)

onLoadpage()

onlyNumber(event)

removeChar(event)

## surveyReport

### main.js

onLoadPage()

getSurvey()

getSurveyIndex()

createData(Groups)

createHeader(h_name, type)

calculatePercent(data)

createBar(data, group)

createTrophy(score)

createEachItemGraph(counts, question, index)

displayQuestion(question, graphContainer, index)

countDuplicate(Scores)

createChart(id, graph)

handleInit()

handleRollOver(e)

onPrint()

## firebase.js

isSignIn()

getSignInEmail()

## readDB.js

getUserList()

getSurveyData(email)

getSurveyList(email)

getSurveyListIndex(email)

getEmailInUserList(user)

## writeDB.js

setEmailToDatabase()

setSurveyToDatabase(type, index)

updateSurveyToDatabase()

onAddEmail(UserList)

onAddSurvey(SurveyList)

onDelSurvey(SurveyList, index)

## SurveyParser.js

getSurveyInDB(SurveyObject)

getSurveyTitleInDB(Survey)

getHostInDB(Survey)

getLectureInDB(Survey)

getSurveyeeCountInDB(Survey)

getDateInDB(Survey)

getGroupsInDB(Survey)

getCategoriesInDB(Group)

getItemsInDB(Category)

getScore(Item)

getGroupTitleInDB(Group)

getCategoryTitleInDB(Category)

getItemTitleInDB(Item)

getArrayForJson(JSONObject)

ObjectToJSON(Object)

## getparameter.js

getParameter()

setGetParameter()

## loadPage.js

loadManagePage()

loadSignInPage()

loadCreateSurveyPage()

loadInputResultPage()

loadSignupPage()

loadSignupPage()

loadSurveyReportPage()

showLoading()

noneLoading()