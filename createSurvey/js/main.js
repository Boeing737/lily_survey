
var Survey = new Map();
var Group;
var Category;
var selected_type;
var SurveyIndex;

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        showLoading();
        startgetDB()
            .then(function (success) {
                if (getParameter() != null) {
                    setSurveyList();
                    showGroupList();
                    $(".added_group")[0].childNodes[1].click();
                }
                noneLoading();
            })
            .catch();
        console.log("success");
    } else {
        alert("로그인이 필요합니다.");
        loadSignInPage();
    }
});

function setSurveyList() {
    var Survey = getSurvey();
    var GroupList = getGroupsInDB(Survey);
    $("#title_text")[0].value = Survey["SurveyTitle"];
    $("#host_text")[0].value = Survey["Host"];
    $("#lecturer_text")[0].value = Survey["Lecture"];
    $("#date_text")[0].value = Survey["Date"];
    GroupList.forEach(function (Groups) {
        var GroupTitle = Groups["Group"];
        Group = GroupTitle;
        console.log(GroupTitle);
        AddGroup(GroupTitle);
        var CategoryList = getCategoriesInDB(Groups);
        CategoryList.forEach(function (Categories) {
            var CategoryTitle = Categories["Category"];
            Category = CategoryTitle;
            console.log(CategoryTitle);
            AddCategory(GroupTitle, CategoryTitle)
            var ItemList = getItemsInDB(Categories);
            ItemList.forEach(function (Items) {
                var ItemTitle = Items["Item"];
                console.log(ItemTitle);
                AddItem(GroupTitle, CategoryTitle, ItemTitle);
            })
        })
    })
}

//return :: Object
function getSurvey() {
    SurveyIndex = getSurveyIndex();
    var SurveyObject = getSurveyList(getSignInEmail())[SurveyIndex];
    var Survey = getSurveyInDB(SurveyObject);
    return Survey;
}

function onSelect(event) {
    var selected = event.target;
    var selected_parent = selected.parentNode;
    selected_type = selected_parent.parentNode.parentNode.id;

    selected_parent.style.opacity = '1';
    $(selected_parent).siblings(':not(.add_text, .survey_heading)').css('opacity', '0.3');

    if (selected_type === 'group') {
        Group = selected.innerHTML;

        showCategoryList(Group);
        var top_category = $(".added_category");
        if (top_category.length > 0) {
            top_category[0].childNodes[1].click();
        } else {
            $(".added_item").remove();
        }
        console.log(getCategoryTitleList(Group));
    } else if (selected_type === 'category') {
        Category = selected.innerHTML;

        showItemList(Group, Category);
        console.log(getItemTitleList(Group, Category));
    }
}

function showGroupList() {
    $(".added_group").remove();

    var GroupTitleList = getGroupTitleList();
    GroupTitleList.forEach(function (groupTitle) {
        onAddData("G", groupTitle);
    })
}

function showCategoryList(Group) {
    $(".added_category").remove();

    var CategoryTitleList = getCategoryTitleList(Group);
    CategoryTitleList.forEach(function (categoryTitle) {
        onAddData("C", categoryTitle);
    })
}

function showItemList(Group, Category) {
    $(".added_item").remove();

    var ItemTitleList = getItemTitleList(Group, Category);
    ItemTitleList.forEach(function (itemTitle) {
        onAddData("I", itemTitle);
    })
}

function isDuplicate(input, array) {
    for(var i = 0; i < array.length; i++){
        if(input === array[i]){
            alert("중복된 이름이 존재합니다.");
            return true;
        }
    }
    return false;
}

function onAddGroup(event) {
    var keyID = this.getKeyID(event)
    var group_list = getGroupTitleList();
    if (keyID == 13) {
        var target = $(event.target)[0];
        var group_input = target.value;
        target.value = "";
        if(isDuplicate(group_input, group_list)) {
            return;
        }
        else if(group_input != null && group_input.trim() != '') {
            var new_group = onAddData("G", group_input);
            AddGroup(group_input);
            new_group.childNodes[1].click();
        }
        else {
            return;
        }
    }
}

function onAddCategory(event) {
    var keyID = this.getKeyID(event);
    if (keyID == 13) {
        if (selected_type) {
            var target = $(event.target)[0];
            var category_input = target.value;
            var category_list = getCategoryTitleList(Group);
            target.value = "";
            if(isDuplicate(category_input, category_list)){
                return;
            }
            else if(category_input != null && category_input.trim() != '') {
                var new_category = onAddData("C", category_input);
                AddCategory(Group, category_input);
                new_category.childNodes[1].click();
            }
            else {
                return;
            }
        }
        else {
            alert('group을 먼저 선택하세요.');
        }
    }
}

function onAddItem(event) {
    var keyID = this.getKeyID(event);
    if (keyID == 13) {
        if (selected_type) {
            if (selected_type === 'category' || selected_type === 'item') {
                var target = $(event.target)[0];
                var item_input = target.value;
                target.value = "";
                if (item_input != null && item_input.trim() != '') {
                    var new_item = onAddData("I", item_input);
                    AddItem(Group, Category, item_input);
                    new_item.click();
                }
            } else {
                alert('category를 먼저 선택하세요.');
            }
        } else {
            alert('group를 먼저 선택하세요.');
        }
    }
}

function onAddData(Selected, input) {
    var item_data;
    var item_container = document.createElement('p');
    var new_data_container = document.createElement('div');
    var container = document.getElementsByClassName('content_area');
    var remove_btn = document.createElement('img');
    remove_btn.src = "./image/remove.png";
    var current_container;
    if (Selected == "G") {
        current_container = container[0];
        new_data_container.className = 'added_group';
        item_data = createGroupOrCategory(new_data_container, input, remove_btn);
        remove_btn.className = 'group_rm_btn';
        container[0].scrollTop = container[0].scrollHeight;
    } else if (Selected == "C") {
        current_container = container[1];
        new_data_container.className = 'added_category';
        item_data = createGroupOrCategory(new_data_container, input, remove_btn);
        remove_btn.className = 'category_rm_btn';
        container[1].scrollTop = container[1].scrollHeight;
    } else if (Selected == "I") {
        current_container = container[2];
        item_container.className = 'added_item';
        item_data = createItemContents(input, item_container, item_container.className, remove_btn);
        remove_btn.className = 'item_rm_btn';
        container[2].scrollTop = container[2].scrollHeight;
    }
    remove_btn.setAttribute('onclick', 'onClickRemove(event)');
    //item_data.appendChild(remove_btn);
    current_container.appendChild(item_data);
    return item_data;
}

function createGroupOrCategory(wrapper, input, remove) {
    var input_text = document.createElement('div');
    var absolute_wrapper = document.createElement('div');
    absolute_wrapper.className = 'absolute_wrapper';
    absolute_wrapper.appendChild(remove);
    wrapper.appendChild(absolute_wrapper);
    wrapper.appendChild(input_text);
    input_text.innerHTML = input;
    input_text.className = 'text';
    input_text.setAttribute('onclick', 'onSelect(event)');
    return wrapper;
}

function createItemContents(input, item_wrapper, this_class, remove) {
    var item_text = document.createElement('div');
    var item_index = document.createElement('div');
    var absolute_wrapper_item = document.createElement('div');
    absolute_wrapper_item.className = 'item_absolute';
    item_text.innerHTML = input;
    item_text.className = 'item_text';
    item_index.innerHTML = getIndexOfItem(this_class.split(" ")[1]) + '.';
    item_index.className = 'item_index';
    item_wrapper.className = this_class;
    absolute_wrapper_item.appendChild(remove);
    item_wrapper.appendChild(item_index);
    item_wrapper.appendChild(item_text);
    item_wrapper.appendChild(absolute_wrapper_item);
    return item_wrapper;
}

function getIndexOfItem(class_name) {
    var items = $(".added_item");
    return items.length + 1;
}

function AddGroup(GroupTitle) {
    Survey.set(GroupTitle, new Map());
}

function AddCategory(GroupTitle, CategoryTitle) {
    var GroupValue = getCategories(GroupTitle);
    GroupValue.set(CategoryTitle, new Array());
}

function AddItem(GroupTitle, CategoryTitle, ItemTitle) {
    var CategoryValue = getItems(GroupTitle, CategoryTitle);
    CategoryValue.push(ItemTitle);
}

//return :: MAPS
function getCategories(GroupTitle) {
    return Survey.get(GroupTitle);
}

//return :: MAPS
function getItems(GroupTitle, CategoryTitle) {
    var GroupValue = getCategories(GroupTitle);
    return GroupValue.get(CategoryTitle);
}

//return :: String
function getSurveyTitle() {
    return $("#title_text").val();
}

//return :: String
function getHost() {
    return $("#host_text").val();
}

//return :: String
function getLecture() {
    return $("#lecturer_text").val();
}

//return :: String
function getDate() {
    return $("#date_text").val();
}

//return :: Array
function getGroupTitleList() {
    return Array.from(Survey.keys());
}

//return :: Array
function getCategoryTitleList(GroupTitle) {
    var CategoryTitleList = getCategories(GroupTitle).keys();
    return Array.from(CategoryTitleList);
}

//return :: Array
function getItemTitleList(GroupTitle, CategoryTitle) {
    var ItemTitleList = getItems(GroupTitle, CategoryTitle);
    return Array.from(ItemTitleList);
}

function MakeSurveyJSON() {
    var GroupTitleList = getGroupTitleList();
    var JSONData = {};
    var Survey = {};
    Survey["Groups"] = [];

    GroupTitleList.forEach(function (GroupTitle) {
        var Group = {};
        Group["Categories"] = [];
        Group["Group"] = GroupTitle;
        var CategoryTitleList = getCategoryTitleList(GroupTitle);

        CategoryTitleList.forEach(function (CategoryTitle) {
            var Category = {};
            Category["Items"] = [];
            Category["Category"] = CategoryTitle;
            var ItemTitleList = getItemTitleList(GroupTitle, CategoryTitle);

            ItemTitleList.forEach(function (ItemTitle) {
                var Item = {};
                Item["Item"] = ItemTitle;
                Item["Score"] = [];

                Category["Items"].push(Item);
                console.log(ItemTitle);
            });
            Group["Categories"].push(Category);
        });
        Survey["Groups"].push(Group);
    });

    Survey["SurveyeeCount"] = 0;
    Survey["SurveyTitle"] = getSurveyTitle();
    Survey["Date"] = getDate();
    Survey["Host"] = getHost();
    Survey["Lecture"] = getLecture();
    Survey["Comments"] = [];

    JSONData["Survey"] = Survey;

    var JSONString = JSON.stringify(JSONData);
    var JSONData = JSON.parse(JSONString);  //json 결과

    console.log(JSONString);
    return JSONData;
}

function checkDateFormat(input_date) {
    var format = /^\d{4}-\d{2}-\d{2}$/; // yyyy-mm-dd 형식
    // date format check for users not using Google Chrome
    if (!input_date.match(format)) {
        alert('Date format mismatch');
        return false;
    }
    var date = new Date(input_date);
    if (!date.getTime()) {
        alert('Wrong date');
        return false;
    }
    return date.toISOString().slice(0, 10) === input_date;
}

function validationCheck() {
    var title_text = document.getElementById('title_text').value;
    var host_text = document.getElementById('host_text').value;
    var lecturer_name = document.getElementById('lecturer_text').value;
    var date_text = document.getElementById('date_text').value;

    //showLoading();

    if (title_text.trim() === '') {
        noneLoading();
        alert('주제를 입력하세요.');
        return;
    }
    if (host_text.trim() === '') {
        noneLoading();
        alert('주최를 입력하세요.');
        return;
    }
    if (lecturer_name.trim() === '') {
        noneLoading();
        alert('강사 이름을 입력하세요.');
        return;
    }
    if (date_text === '' || !checkDateFormat(date_text)) {
        noneLoading();
        alert('올바른 날짜를 입력하세요.');
        return;
    }
    if(!checkEmptyElement()){
        return;
    }
    else { //otherwise send the survey data
        onSubmit();
    }
}

function checkEmptyElement() {
    var GroupTitleList = getGroupTitleList();
    if(GroupTitleList.length === 0){
        alert('설문 항목을 입력하세요.');
        return false;
    }
    try {
        GroupTitleList.forEach(function (GroupTitle) {
            Group["Group"] = GroupTitle;
            var CategoryTitleList = getCategoryTitleList(GroupTitle);
            if(CategoryTitleList.length === 0){
                throw '모든 항목을 추가하세요.';
            }
            CategoryTitleList.forEach(function (CategoryTitle) {
                Category["Category"] = CategoryTitle;
                var ItemTitleList = getItemTitleList(GroupTitle, CategoryTitle);
                if(ItemTitleList.length === 0){
                    throw '질문을 작성해주세요.';
                }
            });
        });
    } catch (error) {
        alert(error);
        return false;
    }
    return true;
}

function onSubmit() {
    setSurveyToDatabase("add", SurveyIndex)
        .then(function (success) {
            loadManagePage();
        })
        .catch(function (error) {
            noneLoading();
            console.log(error);
        });
}

function onClickRemove(event){
    var target = event.target;
    var className = target.className;
    var index;
    if(className == "group_rm_btn"){
        index = $(".group_rm_btn").index(target);
        removeGroup(index);
    }else if(className == "category_rm_btn"){
        index = $(".category_rm_btn").index(target);
        removeCategory(index);
    }else if(className == "item_rm_btn"){
        index = $(".item_rm_btn").index(target);
        removeItem(index);
    }
}

// TODO :: 추후 아래 3개의 함수 최적화 필요!!!!
function removeGroup(index) {
    var seleted_group = $(".added_group")[index];
    Survey.delete(seleted_group.childNodes[1].innerHTML);
    seleted_group.remove();
    var test = $(".added_group");
    if (test.length == 0) {
        // $('.added_category, .added_item').hide();
        $(".added_category").remove();
        $(".added_item").remove();
        Group = "";
        Category = "";
        selected_type = "";
    } else {
        test[0].childNodes[1].click();
    }
}

function removeCategory(index) {
    var seleted_category = $(".added_category")[index];
    Survey.get(Group).delete(seleted_category.childNodes[1].innerHTML);
    showCategoryList(Group);
    var test = $(".added_category");
    if (test.length == 0) {
        // $('.added_item').hide();
        $(".added_item").remove();
        selected_type = "group";
        Category = "";
    } else {
        test[0].childNodes[1].click();
    }
}

// TODO :: 현재 설문 LIST에서 앞에 숫자 변경이 안됨
function removeItem(index) {
    Survey.get(Group).get(Category).splice(index, 1);
    showItemList(Group, Category);
}

function getKeyID(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    return keyID;0
}