var Database = firebase.database().ref('user');
var UserList = [];

function startgetDB() {
    return Database.once('value', function (users) {
        var JSONUsers = ObjectToJSON(users);
        UserList = getArrayForJson(JSONUsers);
    });
}

//return :: Array
function getUserList(){
    return UserList;
}

//return :: Array [0] : SurveyList, [1] : index
function getSurveyData(email){
    // var UserList = getUserList();
    var SurveyList = [];
    var index = -1;
    UserList.forEach(function(user, i){
       if(email === getEmailInUserList(user)){
           SurveyList = getArrayForJson(user["SurveyList"]);
           index = i;
           return;
       }
    });
    return [SurveyList, index];
}

//return :: Array
function getSurveyList(email){
    return getSurveyData(email)[0];
}

//return :: index
function getSurveyListIndex(email){
    return getSurveyData(email)[1];
}

//return :: String
function getEmailInUserList(user){
    return user["userEmail"];
}