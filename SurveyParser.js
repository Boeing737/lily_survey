function getSurveyInDB(SurveyObject) {
    return SurveyObject["Survey"];
}

//return :: String
function getSurveyTitleInDB(Survey) {
    return Survey["SurveyTitle"];
}

//return :: String
function getHostInDB(Survey) {
    return Survey["Host"];
}

//return :: String
function getLectureInDB(Survey) {
    return Survey["Lecture"];
}

//return :: String
function getSurveyeeCountInDB(Survey) {
    return Survey["SurveyeeCount"];
}

//return :: String
function getDateInDB(Survey) {
    return Survey["Date"];
}

//return :: Array
function getGroupsInDB(Survey) {
    return getArrayForJson(Survey["Groups"]);
}

//return :: Array
function getCategoriesInDB(Group) {
    return getArrayForJson(Group["Categories"]);
}

//return :: Array
function getItemsInDB(Category) {
    return getArrayForJson(Category["Items"]);
}

//return :: Array
function getScore(Item) {
    return getArrayForJson(Item["Score"]);
}

//return :: String
function getGroupTitleInDB(Group) {
    return Group["Group"];
}

//return :: String
function getCategoryTitleInDB(Category) {
    return Category["Category"];
}

//return :: String
function getItemTitleInDB(Item) {
    return Item["Item"];
}

//return :: Array
function getArrayForJson(JSONObject) {
    var array = [];

    for (var i in JSONObject) {
        array.push(JSONObject[i]);
    }

    return array;
}

function ObjectToJSON(Object) {
    var StrObject = JSON.stringify(Object);
    var JSONObject = JSON.parse(StrObject);
    return JSONObject;
}