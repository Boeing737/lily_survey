

var modal = document.createElement('div');
var close = document.createElement('span');
var btn = document.getElementById('myBtn');
var body = document.getElementsByTagName('body')[0];
var content = document.createElement('div');
var modal_body = document.createElement('div');
var text = document.createElement('p');
var footer = document.createElement('footer');

modal.className = 'modal';
content.className = 'modal-content';
close.className = 'close';
close.innerHTML = '&times;';
modal_body.className = 'modal-body';
footer.setAttribute('onclick', 'confirm();');

modal_body.appendChild(text);
content.appendChild(close);
content.appendChild(modal_body);
content.appendChild(footer);
modal.appendChild(content);
body.appendChild(modal);

close.onclick = function() {
    modal.style.display = "none";
}

// modal 이외의 다른부분을 누르면 창 Close
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// function test(){
//     footer.onclick = function() {
//         alert('true');
//         modal.style.display = "none";
//         return true;
//     }
// }

// function confirm(){
//     alert('YES!!');
//     modal.style.display = "none";
//     return true;
// }

// function openModal(input) {
//     text.innerHTML = input;
//     modal.style.display = "block";
//     return confirm();
// }

// function run(){
//     openModal().done(function(){
//         alert('done');
//     });
// }

